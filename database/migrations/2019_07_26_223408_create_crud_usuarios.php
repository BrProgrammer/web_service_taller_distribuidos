<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrudUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigIncrements('nombres');
            $table->bigIncrements('apellidos');
            $table->bigIncrements('cedula');
            $table->bigIncrements('genero');
            $table->bigIncrements('correo');
            $table->bigIncrements('clave');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crud');
    }
}
