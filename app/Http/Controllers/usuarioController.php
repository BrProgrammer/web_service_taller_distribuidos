<?php

namespace App\Http\Controllers;

use App\usuarioModel;
use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\DB;
use Mail;
use Redirect;
use Session;


class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return json_encode(usuarioModel::all());
        $usuarios = usuarioModel::all();
        return view('user.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $user_g = new usuarioModel();
        $user_g->nombres=$request->nombres;
        $user_g->apellidos=$request->apellidos;
        $user_g->cedula=$request->cedula;
        $user_g->genero=$request->genero;
        $user_g->correo=$request->correo;
        $user_g->clave=bcrypt($request->nombres);
        $user_g->save();
        return redirect('usuarios');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = usuarioModel::find($id);
        return view('user.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = usuarioModel::find($id);
        return view('user.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $usuarioGuardar= usuarioModel::find($id);
        $usuarioGuardar->nombres=$request->nombres;
        $usuarioGuardar->apellidos=$request->apellidos;
        $usuarioGuardar->cedula=$request->cedula;
        $usuarioGuardar->genero=$request->genero;
        $usuarioGuardar->correo=$request->correo;
        $usuarioGuardar->save();

        return redirect('usuarios');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        usuarioModel::find($id)->delete();
        return redirect('usuarios');
    }
}
