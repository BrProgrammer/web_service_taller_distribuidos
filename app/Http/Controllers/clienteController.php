<?php

namespace App\Http\Controllers;

use App\clienteModel;
use App\usuarioModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class clienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return json_encode(clienteModel::all());


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $clienteGuardar = new clienteModel();
            $clienteGuardar->nombres=$request->nombres;
            $clienteGuardar->apellidos=$request->apellidos;
            $clienteGuardar->cedula=$request->cedula;
            $clienteGuardar->telefono=$request->telefono;
            $clienteGuardar->direccion=$request->direccion;
            $clienteGuardar->save();
            DB::commit();
            return 1;
        }catch(\Illuminate\Database\QueryException $e){
            return $e->getMessage();
            DB::rollBack();
            return 10;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\clienteModel  $clienteModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return json_encode(clienteModel::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\clienteModel  $clienteModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\clienteModel  $clienteModel
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $clienteGuardar = clienteModel::find($id);
        $clienteGuardar->nombres=$request->nombres;
        $clienteGuardar->apellidos=$request->apellidos;
        $clienteGuardar->cedula=$request->cedula;
        $clienteGuardar->telefono=$request->telefono;
        $clienteGuardar->direccion=$request->direccion;
        $clienteGuardar->save();
        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\clienteModel  $clienteModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        clienteModel::find($id)->delete();
        return 1;
    }
}
