@extends('user.parent')

@section('main')

    <div class="jumbotron text-center">

        <div align="right">
            <a href="{{route('usuarios.index')}}" class="btn btn-outline-danger">ATRAS</a>
        </div>
            <h3>Nombres: {{$data->nombres}}</h3>
            <h3>Apellidos: {{$data->apellidos}}</h3>
            <h3>Cedula: {{$data->cedula}}</h3>
            <h3>Genero: {{$data->genero}}</h3>
            <h3>Correo: {{$data->correo}}</h3>


    </div>

@endsection
