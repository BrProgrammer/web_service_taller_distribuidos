@extends('user.parent')


@section('main')

    <form method="post" action="{{route('usuarios.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="card" style="margin: 10px 10px 10px 10px; padding: 20px 20px 0px 20px;  margin-top: 150px; background:rgba(51,51,51, 0.5);" >
            <form>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="text" name="nombres" class="form-control input-lg" placeholder="Ingrese Sus Nombres">
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text" name="apellidos" class="form-control input-lg" placeholder="Ingrese Sus Nombres">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="number" name="cedula" class="form-control input-lg" placeholder="Ingrese Su Número De Cédula">
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text" name="genero" class="form-control input-lg" placeholder="Ingrese Su Género">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="email" name="correo" class="form-control input-lg" placeholder="Ingrese Correo Electrónico">
                    </div>
                    <div class="form-group col-md-6">
                        <input type="password" name="clave" class="form-control input-lg" placeholder="Ingrese Su Clave">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <button type="submit" class="btn btn-outline-success col-md-12" value="Add" name="add">REGISTRARSE</button>
                    </div>
                    <div class="form-group col-md-6">
                        <button type="submit" class="btn btn-outline-primary col-md-12" href="{{ route('usuarios.index') }}">CANCELAR</button>
                    </div>
                </div>
            </form>
        </div>
    </form>
@endsection
