@extends('user.parent')

@section('main')


        <div class="row">
            <div class="col">
                <table class="table table-bordered table-hover" >
                    <thead class="thead-dark">
                    <tr>
                        <th >N°</th>
                        <th >NOMBRES</th>
                        <th >APELLIDOS</th>
                        <th >CEDULA</th>
                        <th >GÉNERO</th>
                        <th >CORREO</th>
                        <th class="text-center">
                            <a href="{{route('usuarios.create')}}" class="btn btn-outline-success">AGREGAR</a>
                        </th>
                    </tr>
                    </thead>
                @foreach($usuarios as $user)
                        <tbody>
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->nombres}}</td>
                            <td>{{$user->apellidos}}</td>
                            <td>{{$user->cedula}}</td>
                            <td>{{$user->genero}}</td>
                            <td>{{$user->correo}}</td>
                            <td class="text-center" style="width: auto; display: flex;justify-content: space-around;">
                                <a style="height: 40px;" href="{{route('usuarios.show', $user->id)}}" class="btn btn-outline-info">VER</a>
                                <a  style="height: 40px;"href="{{route('usuarios.edit', $user->id)}}" class="btn btn-outline-warning">EDITAR</a>
                                <form action="{{route('usuarios.destroy', $user->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <a style="height: 40px;" class="btn btn-outline-danger">ELIMINAR</a>
                                </form>
                            </td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>


{{--    {!! $usuarios->link() !!}--}}
@endsection
