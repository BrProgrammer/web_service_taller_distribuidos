@extends('user.parent')

@section('main')

    <div align="right">
        <a href="{{route('usuarios.index')}}" class="btn btn-outline-danger">ATRAS</a>
    </div>

    <br>

    <form method="post" action="{{route('usuarios.update', $data->id)}}"
    enctype="multipart/form-data">

        @csrf
        @method('PATCH')
            <div class="card" style="margin: 10px 10px 10px 10px; padding: 20px 20px 0px 20px;  margin-top: 150px; background:rgba(51,51,51, 0.5);" >
                <form>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="nombres" class="form-control input-lg" value="{{$data->nombres}}">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" name="apellidos" class="form-control input-lg" value="{{$data->apellidos}}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="number" name="cedula" class="form-control input-lg" value="{{$data->cedula}}">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" name="genero" class="form-control input-lg" value="{{$data->genero}}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input type="email" name="correo" class="form-control input-lg" value="{{$data->correo}}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-outline-success col-md-12" value="Add" name="add">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>
    </form>

@endsection
